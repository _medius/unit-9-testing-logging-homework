package com.epam.calculator;

import org.apache.log4j.Logger;

import java.util.Arrays;

/**
 * This class contains methods for arithmetic calculations.
 * @author Mikhail Bogdashin
 */
public class Calculator {

	private static final Logger logger = Logger.getLogger(Calculator.class);

	/**
	 * Performs a math operation of addition
	 * @param oneAddend - the first addend
	 * @param twoAddend - the second addend
	 * @return - sum of two elements
	 */
	public static int sum(int oneAddend, int twoAddend) {
		logger.info("Method sum() called with addends " + oneAddend + ", " + twoAddend + '.');
		return oneAddend + twoAddend;
	}

	/**
	 * Performs a math operation of subtraction
	 * @param minuend - the minuend
	 * @param subtrahend - the subtrahend
	 * @return - difference of the arguments
	 */
	public static int sub(int minuend, int subtrahend) {
		logger.info("Method sub() called with minuend " + minuend + ", and subtrahend " + subtrahend + '.');
		return minuend - subtrahend;
	}

	/**
	 * Performs a math operation of multiplication
	 * @param oneNumber - first multiplier
	 * @param twoNumber - second multiplier
	 * @return - product of two arguments
	 */
	public static int multiply(int oneNumber, int twoNumber) {
		logger.info("Method multiply called with multipliers " + oneNumber + ", " + twoNumber + '.');
		return oneNumber * twoNumber;
	}

	/**
	 * Performs a math operation of division
	 * @param dividend - the dividend
	 * @param divisor - the divisor
	 * @throws IllegalArgumentException if divisor is 0
	 * @return - quotient of two arguments
	 */
	public static double divide(int dividend, int divisor) {
		logger.info("Method divide() called with dividend " + dividend + ", and divisor " + divisor + '.');
		if (divisor == 0) {
			logger.error("Division by 0.");
			throw new IllegalArgumentException("Divisor equals 0.");
		}
		return dividend / divisor;
	}

	/**
	 * Performs a math operation of extracting a square root
	 * @param number - base of square root
	 * @throws IllegalArgumentException if argument is < 0
	 * @return - positive square root of argument
	 */
	public static double squareRootOf(int number) {
		logger.info("Method squareRoot() called with argument " + number + '.');
		if (number < 0) {
			logger.error("Base of square root is " + number + '.');
			throw new IllegalArgumentException("Negative base of square root.");
		}
		return Math.sqrt(number);
	}

	/**
	 * Checks the specified number is prime
	 * @param number - number to check
	 * @throws IllegalArgumentException if argument <= 1
	 * @return - <tt>true</tt> if number is prime
	 */
	public static boolean isPrime(int number) {
		logger.info("Method isPrime() called with argument " + number + '.');
		if (number <= 1) {
			logger.error("Try to check as prime not natural number " + number + '.');
			throw new IllegalArgumentException("Number must be positive integer > 1.");
		}

		if (number % 2 == 0 && number != 2) {
			logger.debug("Method isPrime(). Argument is even number not 2. Returned false.");
			return false;
		}

		for (int i = 3; i*i <= number; i++) {
			logger.debug("Method isPrime(). Inside cycle, i=" + i + '.');
			if (number % i == 0) {
				logger.trace("Method isPrime(). Argument have denominator another " + i + ", returned false.");
				return false;
			}
		}
		return true;
	}

	/**
	 * Calculates the specified element of Fibonacci sequence
	 * @param number - number of element
	 * @throws IllegalArgumentException if the result overflows long
	 * @return - number of Fibonacci sequence
	 */
	public static long getFibonacciNumber(int number) {
		logger.info("Method getFibonacciNumber() called with argument " + number + '.');
		if(number > 92 || number < -92) {
			logger.error("Try to take Fibonacci number by " + number + '.');
			throw new IllegalArgumentException("Requested value is over than max for \"long\" type.");
		}

		long result = 0;

		//initial conditions
		long zeroElement = 0;
		long firstElement = 1;

		if (number == 0) {
			logger.debug("Method getFibonacciNumber(). Returned zeroElement.");
			return zeroElement;
		}

		if (number == 1) {
			logger.debug("Method getFibonacciNumber(). Returned firstElement.");
			return firstElement;
		}

		if (number > 1) {
			for (int i = 2; i < number + 1; i++) {
				logger.debug("Method getFibonacciNumber(). " +
						"Inside cycle with positive number calculations i=" + i + '.');
				result = zeroElement + firstElement;
				zeroElement = firstElement;
				firstElement = result;
				logger.trace("Method getFibonacciNumber(). Positive cycle, parameters i " + i +
						", previous elements " + zeroElement + " " + firstElement + " current element " + result);
			}
		}

		if (number < 0) {
			for (int i = 0; i > number; i--) {
				logger.debug("Method getFibonacciNumber(). " +
						"Inside cycle with negative number calculations i " + i + '.');
				result = firstElement - zeroElement;
				firstElement = zeroElement;
				zeroElement = result;
				logger.trace("Method getFibonacciNumber(). Negative cycle, parameters i " + i +
						", previous elements " + zeroElement + " " + firstElement + " current element " + result);
			}
		}
		return result;
	}

	/**
	 * Method summarizes elements of specified array
	 * @param row - array which elements need to summarize
	 * @return - sum of array elements
	 */
	public static int summarizeNumberRow(int[] row) {
		logger.info("Method summarizeNumberRow() called with array " + Arrays.toString(row));
		return Arrays.stream(row).sum();
	}
}
