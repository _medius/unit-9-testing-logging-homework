package com.epam.calculator;

import org.apache.log4j.Logger;

/**
 * This class contains methods returning predefined arrays to try Mockito and PowerMock features.
 * @author Mikhail Bogdashin
 */
public class NumberRowSupplier {

	private static final Logger logger = Logger.getLogger(NumberRowSupplier.class);

	/**
	 * Returns the predefined array. Final method.
	 * @return - predefined array
	 */
	final int[] getFinalRow() {
		logger.info("Method getFinalRow() called.");
		return new int[]{1, 2, 3};
	}

	/**
	 * Returns the predefined array. Static method.
	 * @return - predefined array
	 */
	static int[] getStaticRow() {
		logger.info("Method getStaticRow() called.");
		return new int[]{4, 5, 6};
	}

	/**
	 * Returns the predefined array. Private method.
	 * @return - predefined array
	 */
	private int[] generateRow() {
		logger.info("Private method generateRow() called.");
		return new int[]{7, 8, 9};
	}

	/**
	 * Provides access to array which returned by private method.
	 * @return - predefined array
	 */
	public int[] getPrivateRow() {
		logger.info("Public method getPrivateRow() called.");
		return generateRow();
	}

	/**
	 * Returns the predefined array. Public method.
	 * @return - predefined array
	 */
	public int[] getRow() {
		logger.info("Method getRow() called.");
		return new int[]{10, 11, 12};
	}
}
