package com.epam.calculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

import static com.epam.calculator.Calculator.*;

public class CalculatorNegativeTest {

	private Random random = new Random();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testDivideByZero() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Divisor equals 0.");
		divide(random.nextInt(), 0);
	}

	@Test
	public void testNegativeBaseSqrt() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Negative base of square root.");
		squareRootOf(-1);
	}

	@Test
	public void testNotNaturalIsPrime() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Number must be positive integer > 1.");
		isPrime(-5);
	}

	@Test
	public void testOneIsPrime() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Number must be positive integer > 1.");
		isPrime(1);
	}

	@Test
	public void testZeroIsPrime() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Number must be positive integer > 1.");
		isPrime(0);
	}

	@Test
	public void testTooBigFibonacci() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Requested value is over than max for \"long\" type.");
		getFibonacciNumber(-93);
		getFibonacciNumber(93);
	}
}
