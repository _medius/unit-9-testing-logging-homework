package com.epam.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static com.epam.calculator.Calculator.*;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(NumberRowSupplier.class)
public class CalculatorPositiveTest {

	private static final int TIMEOUT_FOR_ISPRIME = 100;

	private Random random = new Random();

	@Test
	public void testSum() {
		assertEquals(5, sum(2, 3));
	}

	@Test
	public void testSumNegativeNumbers() {
		assertEquals(-5, sum(-2, -3));
	}

	@Test
	public void testSumDifferentSignNumbers() {
		assertEquals(1, sum(-2, 3));
	}

	@Test
	public void testSub() {
		assertEquals(2, sub(5, 3));
	}

	@Test
	public void testSubNegativeNumbers() {
		assertEquals(-2, sub(-5, -3));
	}

	@Test
	public void testSubDifferentSignNumbers() {
		assertEquals(-8, sub(-5, 3));
	}

	@Test
	public void testMultiply() {
		assertEquals(15, multiply(5, 3));
	}

	@Test
	public void testMultiplyNegativeNumbers() {
		assertEquals(15, multiply(-5, -3));
	}

	@Test
	public void testMultiplyDifferentSignNumbers() {
		assertEquals(-15, multiply(-5, 3));
	}

	@Test
	public void testMultiplyByZero() {

		assertEquals(0, multiply(random.nextInt(), 0));
	}

	@Test
	public void testDivide() {
		assertEquals(5, divide(15, 3));
	}

	@Test
	public void testDivideNegativeNumbers() {
		assertEquals(5, divide(-15, -3));
	}

	@Test
	public void testDivideDifferentSignNumbers() {
		assertEquals(-5, divide(15, -3));
	}

	@Test
	public void testDivideZeroByNumber() {
		assertEquals(0, divide(0, random.nextInt()));
	}

	@Test
	public void testSquareRoot() {
		assertEquals(5, squareRootOf(25));
	}

	@Test
	public void testSquareRootOfZero() {
		assertEquals(0, squareRootOf(0));
	}

	@Test(timeout = TIMEOUT_FOR_ISPRIME)
	public void testIsPrime() {
		assertTrue(isPrime(17));
		assertTrue(isPrime(331));
		assertTrue(isPrime(73));
	}

	@Test(timeout = TIMEOUT_FOR_ISPRIME)
	public void testNotIsPrime() {
		assertFalse(isPrime(16));
		assertFalse(isPrime(333));
		assertFalse(isPrime(1000));
	}

	@Test(timeout = TIMEOUT_FOR_ISPRIME)
	public void testTwoIsPrime() {
		assertTrue(isPrime(2));
	}

	@Test
	public void testFibonacci() {
		assertEquals(0L, getFibonacciNumber(0));
		assertEquals(1L, getFibonacciNumber(1));
		assertEquals(7540113804746346429L, getFibonacciNumber(92));
		assertEquals(806515533049393L, getFibonacciNumber(73));
		assertEquals(-86267571272L, getFibonacciNumber(-54));
		assertEquals(-5702887L, getFibonacciNumber(-34));
	}

	@Test
	public void testRowSummarizer() {
		NumberRowSupplier mockSupplier = Mockito.mock(NumberRowSupplier.class);
		Mockito.when(mockSupplier.getRow()).thenReturn(new int[]{3, 3, 3});
		assertEquals(9, summarizeNumberRow(mockSupplier.getRow()));


		NumberRowSupplier spySupplier = PowerMockito.spy(new NumberRowSupplier());
		try {
			PowerMockito.when(spySupplier, "generateRow").thenReturn(new int[]{5, 5, 5});
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(15, summarizeNumberRow(spySupplier.getPrivateRow()));


		PowerMockito.mockStatic(NumberRowSupplier.class);
		PowerMockito.when(NumberRowSupplier.getStaticRow()).thenReturn(new int[]{9, 9, 9});
		assertEquals(27, summarizeNumberRow(NumberRowSupplier.getStaticRow()));


		NumberRowSupplier powerSupplier = PowerMockito.mock(NumberRowSupplier.class);
		PowerMockito.when(powerSupplier.getFinalRow()).thenReturn(new int[]{10, 10, 10});
		assertEquals(30, summarizeNumberRow(powerSupplier.getFinalRow()));
	}
}
